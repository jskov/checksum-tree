package dk.mada.checksum;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Stream;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.greenrobot.common.hash.Murmur3F;

public class DirChecksum {
    private static final Logger logger = LoggerFactory.getLogger(DirChecksum.class);
    
    public void go(Path dir) throws IOException {
        long start = System.currentTimeMillis();
        getListOfFilesToChecksum(dir);
        long time = System.currentTimeMillis() - start;
        logger.info("Processed in {}ms", time);
    }
    
    private void getListOfFilesToChecksum(Path dir) throws IOException {
        try (Stream<Path> files = Files.walk(dir)) {
            files
                .map(p -> mapToOutputLine(dir, p))
                .sorted()
                .forEach(this::print);
        }
    }
    
    private String mapToOutputLine(Path rootDir, Path file) {
        String relPath = rootDir.relativize(file).toString();
        if (Files.isDirectory(file)) {
            return relPath + "/";
        } else {
            return checksumFile(relPath, file);
        }        
    }
    
    private void print(String s) {
        System.out.println(s);
    }
    
    byte[] buffer = new byte[16*1024];
    private String checksumFile(String relPath, Path file) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Unknown hasher", e);
        }
        
        Murmur3F c = new Murmur3F();
        
        try (BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(file))) {
            int readBytes;
            while((readBytes = bis.read(buffer)) != -1) {
                c.update(buffer, 0, readBytes);
                md.update(buffer, 0, readBytes);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed checksumming file " + file, e);
        }
        
        return String.format("%-120s #%s #mm3f:%s", relPath, DatatypeConverter.printHexBinary(md.digest()).toLowerCase(), c.getValueHexString());
    }

}
