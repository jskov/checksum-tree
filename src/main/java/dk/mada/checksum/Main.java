package dk.mada.checksum;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Inject;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    
    @Inject private DirChecksum dirChecksum;
    
    public static void main(String[] args) {
       Weld weld = new Weld();
       try (WeldContainer container = weld.initialize()) {
           container.select(Main.class).get().run();
       }
    }

    private void run() {
//        Path dir = Paths.get("/opt/photos");
        Path dir = Paths.get("/opt/data/photo-bloks");
//        Path dir = Paths.get("/opt/ogg-collection");
        
        logger.info("Checksum {}", dir);
        try {
            dirChecksum.go(dir);
        } catch (IOException e) {
            logger.warn("Failed checksumming", e);
        }
    }
}
